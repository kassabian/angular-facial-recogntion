(function (angular) {

  // Create all modules and define dependencies to make sure they exist
  // and are loaded in the correct order to satisfy dependency injection
  // before all nested files are concatenated by Gulp

  // Config
  angular.module('ngOxfordFace.config', [])
      .value('ngOxfordFace.config', {
          debug: true
      });

  // Modules
  angular.module('ngOxfordFace.directives', []);
  angular.module('ngOxfordFace.services',
      [
        'ngOxfordFace.services.face-api-wrapper'
      ]);
  angular.module('ngOxfordFace',
      [
          'ngOxfordFace.config',
          'ngOxfordFace.directives',
          'ngOxfordFace.services',
          'ngResource'
      ]);

})(angular);

angular.module('ngOxfordFace.services.face-api-wrapper', [])
.provider('ProjectOxfordFace', function($httpProvider) {
	var primaryKey;
	var apiUrl = 'https://api.projectoxford.ai/face/v1.0/';

	return {
		setPrimaryKey: function(pKey) {
			// If array of primary keys has been passed in, randomly select one
			if(Array.isArray(pKey)) {
				primaryKey = pKey[Math.floor(Math.random() * pKey.length)];
			} else {
				primaryKey = pKey;
			}
			// Set HTTP Headers
			$httpProvider.defaults.headers.common['Content-Type'] = 'application/json';
			$httpProvider.defaults.headers.common['Ocp-Apim-Subscription-Key'] = primaryKey;
		},

		setApiUrl: function(url) {
			apiUrl = url || 'https://api.projectoxford.ai/face/v1.0/';
		},

		$get: function($resource) {
			var Face = $resource(apiUrl + ':action/:id');

			return {
				/* Face List Methods
				** 1. faceLists()
				** 2. face()
        ** 3. personGroup()
        ** 4. person()
				**/
				faceLists: faceLists,
				face: faceActions,
				personGroup: personGroup,
        person: person
			}; //***END $get return{}

			// START - faceLists()
			function faceLists() {
				// Override Face $resource configuration
				var Face = $resource(apiUrl + 'facelists/:id/:action/:actionId', {}, {
					create: {
						method: 'PUT'
					},
					update: {
						method: 'PATCH'
					}
				});

				return {
					create: function(faceListId, name, userData) {
						return Face.create({id: faceListId}, {
							name: name,
							userData: userData
						});
					},
					get: function(id) {
						return Face.get({id: id});
					},
					query: function() {
						return Face.query();
					},
					update: function(faceListId, name, userData) {
						return Face.update({id: faceListId}, {
							name: name,
							userData: userData
						});
					},
					delete: function(faceListId) {
						return Face.delete({id: faceListId});
					},
					addFace: function(faceListId, userData, targetFace, imgUrl) {
						userData = userData || '';
						targetFace = targetFace || '';

						return Face.save({id: faceListId, action: 'persistedFaces', userData: userData, targetFace: targetFace}, {
							url: imgUrl
						});
					},
					deleteFace: function(faceListId, persistedFaceId) {
						return Face.delete({id: faceListId, action: 'persistedFaces', actionId: persistedFaceId});
					}
				};
			} // ***END faceLists()

			// START - faceActions()
			function faceActions() {
				var Face = $resource(apiUrl + ':action/:id', {}, {
					detect: {
						method: 'POST',
						isArray: true
					},
					similar: {
						method: 'POST',
						isArray: true
					}
				});
				return {
					detect: function(imgUrl, params) {
						params.action = 'detect';
						return Face.detect(params, {
							url: imgUrl
						});
					},
					similar: function(body) {
						return Face.similar({action: 'findsimilars'}, body);
					},
					group: function(body) {
						return Face.save({action: 'group'}, body);
					},
					identify: function(body) {
						return Face.save({action: 'identify'}, body);
					},
					verify: function(body) {
						return Face.save({action: 'verify'}, body);
					}
				}; // ***END return {}
			} // ***END faceActions()

      function personGroup() {
				var Face = $resource(apiUrl + 'persongroups/:groupId/:action', {}, {
          create: {
            method: 'PUT'
          },
          update: {
            method: 'PATCH'
          }
        });
				return {
          create: function(personGroupId, body) {
            return Face.create({groupId: personGroupId}, {name: body.name, userData: body.userData});
          },
          get: function(personGroupId) {
            return Face.get({groupId: personGroupId});
          },
          getTraining: function(personGroupId) {
            return Face.get({groupId: personGroupId, action: 'training'});
          },
          getGroups: function() {
            return Face.query();
          },
          update: function(personGroupId, body) {
            return Face.update({groupId: personGroupId}, {
              name: body.name,
              userData: body.userData
            });
          },
          delete: function(personGroupId) {
            return Face.delete({groupId: personGroupId});
          },
          train: function(personGroupId) {
            return Face.save({groupId: personGroupId, action: 'train'}, {});
          }
				};
			} // ***END personGroup()

      function person(personGroupId, body) {
        var Face = $resource(apiUrl + 'persongroups/:groupId/:noun/:nounId/:action/:actionId', {}, {
          update: {
            method: 'PUT'
          }
        });
        return {
          create: function(personGroupId) {
            return Face.save({groupId: personGroupId}, {
              name: body.name,
              userData: body.userData
            });
          },
          get: function(personGroupId, personId) {
            return Face.get({groupId: personGroupId, noun: 'persons', nounId: personId});
          },
          update: function(personGroupId, personId, body) {
            Face.update({groupId: personGroupId, noun: 'persons', nounId: personId}, {
              name: body.name,
              userData: body.userData
            });
          },
          delete: function(personGroupId, personId) {
            Face.delete({groupId: personGroupId, noun: 'persons', nounId: personId});
          },
          addFace: function(personGroupId, personId, params, body) {
            return Face.save({
              groupId: personGroupId,
              noun: 'persons',
              nounId: personId,
              action: 'persistedFaces',
              userData: params.userData,
              targetFace: params.targetFace
            }, {url: body.url});
          },
          getFace: function(personGroupId, personId, persistedFaceId) {
            return Face.get({
              groupId: personGroupId,
              noun: 'persons',
              nounId: personId,
              action: 'persistedFaces',
              actionId: persistedFaceId
            });
          },
          updateFace: function(personGroupId, personId, persistedFaceId, body) {
            return Face.update({
              groupId: personGroupId,
              noun: 'persons',
              nounId: personId,
              action: 'persistedFaces',
              actionId: persistedFaceId
            }, {userData: body.userData});
          },
          deleteFace: function(personGroupId, personId, persistedFaceId) {
            return Face.delete({
              groupId: personGroupId,
              noun: 'persons',
              nounId: personId,
              action: 'persistedFaces',
              actionId: persistedFaceId
            }, {});
          },
          listPersonGroup: function(personGroupId) {
            return Face.query({groupId: personGroupId, noun: 'persons'});
          }
        }; // ***END return{}

      } // ***END person()

		} // ***END $get()

	}; // ***END return {}

}); // ***END provider()
