## Angular Project Oxford Face API Wrapper

Using Angular along with libraries like ngResource, This project serves as a wrapper that makes working with Microsoft’s Project Oxford Face API (V1.0) a breeze.

The following collections/resources along with all of their HTTP actions have been fully implemented.

- Face
- Face List
- Person
- Person Group


For general API documentation/reference click the following link - [Microsoft Face API Documentation](https://dev.projectoxford.ai/docs/services/563879b61984550e40cbbe8d/operations/563879b61984550f30395236)
