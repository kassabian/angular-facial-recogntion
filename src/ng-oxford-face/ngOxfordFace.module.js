(function (angular) {

  // Create all modules and define dependencies to make sure they exist
  // and are loaded in the correct order to satisfy dependency injection
  // before all nested files are concatenated by Gulp

  // Config
  angular.module('ngOxfordFace.config', [])
      .value('ngOxfordFace.config', {
          debug: true
      });

  // Modules
  angular.module('ngOxfordFace.directives', []);
  angular.module('ngOxfordFace.services',
      [
        'ngOxfordFace.services.face-api-wrapper'
      ]);
  angular.module('ngOxfordFace',
      [
          'ngOxfordFace.config',
          'ngOxfordFace.directives',
          'ngOxfordFace.services',
          'ngResource'
      ]);

})(angular);
