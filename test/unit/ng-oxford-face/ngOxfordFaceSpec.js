'use strict';

describe('', function() {

  var module;
  var dependencies;
  dependencies = [];

  var hasModule = function(module) {
  return dependencies.indexOf(module) >= 0;
  };

  beforeEach(function() {

  // Get module
  module = angular.module('ngOxfordFace');
  dependencies = module.requires;
  });

  it('should load config module', function() {
    expect(hasModule('ngOxfordFace.config')).to.be.ok;
  });

  

  
  it('should load directives module', function() {
    expect(hasModule('ngOxfordFace.directives')).to.be.ok;
  });
  

  
  it('should load services module', function() {
    expect(hasModule('ngOxfordFace.services')).to.be.ok;
  });
  

});